import nltk
import string

level = 3
p = "The death of many authors"

def rhyme(inp, level):
	entries = nltk.corpus.cmudict.entries()
	syllables = [(word, syl) for word, syl in entries if word == inp]
	rhymes = []
	for (word, syllable) in syllables:
		rhymes += [word for word, pron in entries if pron[-level:] == syllable[-level:]]
		return set(rhymes)

p = "hallo hoe gaat het daar"

def countWords():
  	words = string.split(p)
  	wordCount = len(words)
	return wordCount

print "word?"
word = raw_input()
print rhyme(word, level)
print countWords()
