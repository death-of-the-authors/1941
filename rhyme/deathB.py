import nltk
import string
from random import *

#level of precision in rhyme. 1 is messy, 5 is precise
level = 4

#variables
sentences_select_virginia = []
sentences_select_rabindranath = []
lastwords_virginia = []
lastwords_rabindranath = []
y = 1
i = 1
sentences_done = []
exclude = set(string.punctuation)

output = open("coupletB.txt", "w")

#preparing sentences
virginia1 = open('Virginia1.txt', 'r')
virginia1 = virginia1.read()
virginia1 = virginia1.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia2 = open('Virginia2.txt', 'r')
virginia2 = virginia2.read()
virginia2 = virginia2.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia3 = open('Virginia3.txt', 'r')
virginia3 = virginia3.read()
virginia3 = virginia3.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

rabindranath1 = open('Rabindranath1.txt', 'r')
rabindranath1 = rabindranath1.read()
rabindranath1 = rabindranath1.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

rabindranath2 = open('Rabindranath2.txt', 'r')
rabindranath2 = rabindranath2.read()
rabindranath2 = rabindranath2.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

rabindranath3 = open('Rabindranath3.txt', 'r')
rabindranath3 = rabindranath3.read()
rabindranath3 = rabindranath3.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

rabindranath4 = open('Rabindranath4.txt', 'r')
rabindranath4 = rabindranath4.read()
rabindranath4 = rabindranath4.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

rabindranath5 = open('Rabindranath5.txt', 'r')
rabindranath5 = rabindranath5.read()
rabindranath5 = rabindranath5.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

sentences_virginia = sent_tokenizer.tokenize(virginia1) + sent_tokenizer.tokenize(virginia2) + sent_tokenizer.tokenize(virginia3)
sentences_rabindranath = sent_tokenizer.tokenize(rabindranath1) + sent_tokenizer.tokenize(rabindranath2) + sent_tokenizer.tokenize(rabindranath3) + sent_tokenizer.tokenize(rabindranath4) + sent_tokenizer.tokenize(rabindranath5)

def rhyme(inp, level):
    entries = nltk.corpus.cmudict.entries()
    syllables = [(word, syl) for word, syl in entries if word == inp]
    try:
        rhymes = []
        for (word, syllable) in syllables:
            rhymes += [word for word, pron in entries if pron[-level:] == syllable[-level:]]
            return set(rhymes)
    except:
         pass

f = open("sentences_virginia.txt", "w")
for sentence_virginia in sentences_virginia:
    words_virginia = string.split(sentence_virginia)
    wordCount = len(words_virginia)
    if wordCount > 8 and wordCount < 11:
        sentences_select_virginia.append(sentence_virginia)
        f.write(sentence_virginia + "\n")
        lastword_virginia = sentence_virginia.split()[-1]
        lastword_virginia = ''.join(ch for ch in lastword_virginia if ch not in exclude)
        lastwords_virginia.append(lastword_virginia.lower())

f = open("sentences_rabindranath.txt", "w")
for sentence_rabindranath in sentences_rabindranath:
    words_rabindranath = string.split(sentence_rabindranath)
    wordCount = len(words_rabindranath)
    if wordCount > 8 and wordCount < 11:
        sentences_select_rabindranath.append(sentence_rabindranath)
        f.write(sentence_rabindranath + "\n")
        lastword_rabindranath = sentence_rabindranath.split()[-1]
        lastword_rabindranath = ''.join(ch for ch in lastword_rabindranath if ch not in exclude)
        lastwords_rabindranath.append(lastword_rabindranath.lower())

while y < 22:
	x = 1
	sentences_done = []
	print
	print "Couplet ", y
	print
	output.write("The Death of The Authors B: Couplet " + str(y) + "\n--------\n\n")
	y += 1
	while x < 8:
		if i == 1:
			sentence_r = choice(sentences_select_virginia)
			index_r = sentences_select_virginia.index(sentence_r)
			lastwords_virginia.remove(lastwords_virginia[index_r])
			sentences_select_virginia.remove(sentence_r)
			if sentence_r not in sentences_done:
				i = 2
		if i == 2:
			words_rabindranath = string.split(sentence_r)
			lastword_r = ''.join(ch for ch in words_rabindranath[-1] if ch not in exclude)
			rhymewords = rhyme(lastword_r, level)
			if rhymewords:
				for rhymeword in rhymewords:
					if lastword_r != rhymeword:
						if rhymeword in lastwords_rabindranath and sentence_r not in sentences_done:
							sentences_done.append(sentence_r)
							index_v = lastwords_rabindranath.index(rhymeword)
							sentence_v = sentences_select_rabindranath[index_v]
							output.write(sentence_r + "\n" + sentence_v + "\n\n")
							print x, "------"
							print sentence_r
							print sentence_v
							print
							x += 1
		i = 1

output.close()
