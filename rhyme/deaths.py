import nltk
import string
from random import *
import os
import time

virginia1 = open('Virginia1.txt', 'r')
virginia1 = virginia1.read()
virginia1 = virginia1.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia2 = open('Virginia2.txt', 'r')
virginia2 = virginia2.read()
virginia2 = virginia2.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia3 = open('Virginia3.txt', 'r')
virginia3 = virginia3.read()
virginia3 = virginia3.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

#S output file
output = open("result.txt", "w")

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
#S Puts all sentences of all the texts in one array
sentences = sent_tokenizer.tokenize(virginia1) + sent_tokenizer.tokenize(virginia2) + sent_tokenizer.tokenize(virginia3)

sentences_select = []
sentences_done = []
lastwords = []
level = 1
exclude = set(string.punctuation)

def rhyme(inp, level):
    #S inp = last word from selected sentence
    #S words from NLTK dictionnary
    entries = nltk.corpus.cmudict.entries()
    #S matches selected word in NLTK dictionary and gets its phonetic syllables from it
    syllables = [(word, syl) for word, syl in entries if word == inp]
    try:
        #S Prints the ending syllables of the selected word; if there's an IndexError, it means it was not found in NLTK dictionary and then goes out the function
        #print syllables[0][1][-level:]
        #S if word is longer than chosen level; maybe not necessary?
        rhymes = []
        for (word, syllable) in syllables:
            #S Looks for words in dictionary which ending phonetic syllable matches the ones from the selected word
            rhymes += [word for word, pron in entries if pron[-level:] == syllable[-level:]]
            #print rhymes
            return set(rhymes)
    except:
         pass

f = open("sentences.txt", "w")
for sentence in sentences:
    words = string.split(sentence)
    wordCount = len(words)
    if wordCount == 10:
        #S Selects sentences with 10 words
        sentences_select.append(sentence)
        f.write(sentence + "\n")
        lastword = sentence.split()[-1]
        lastword = ''.join(ch for ch in lastword if ch not in exclude)
        lastwords.append(lastword.lower())

found = "NO"

x = 1
while x < 3:
    i = 1
    a = 1
    while i < 3:
        #S i is first equal to 1, so I put this block of code up there
        if i == 1:
            #print "select sentence"
            sentence1 = choice(sentences_select)
            #S removes selected sentence from the list so that it's not selected twice, and the lastword corresponding to it.
            index1 = sentences_select.index(sentence1)
            lastwords.remove(lastwords[index1])
            sentences_select.remove(sentence1)
            #print sentence1
            #S I would put this if command before the choice so that one sentence is not chosen twice?
            if sentence1 not in sentences_done:
                # print "selected a sentence"
                sentences_done.append(sentence1)
                i += 1
        if i == 2:
            a += 1
            #print "started a rhyming loop", a
            words = string.split(sentence1)
            lastword1 = ''.join(ch for ch in words[9] if ch not in exclude)
            rhymewords = rhyme(lastword1, level)
            #print rhymewords
            if rhymewords:
                for rhymeword in rhymewords:
                    #print "selected rhymeword: ", rhymeword
                    if lastword1 != rhymeword and found == "NO":
                        #print "started looking at sentences to rhyme"
                        if rhymeword in lastwords:
                            #S search if rhymeword is a last word of a sentence
                            #print rhymeword
                            index2 = lastwords.index(rhymeword)
                            sentence2 = sentences_select[index2]
                            #S Writes sentences to the file "result.txt"
                            output.write(sentence1 + "\n" + sentence2 + "\n")
                            found = "YES"
                            print x, "------"
                            print sentence1
                            print sentence2
                            i += 1
                            x += 1
            #print "no rhymewords found; select another sentence"
            i = 1

output.close()
