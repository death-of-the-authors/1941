import nltk
import string
from random import *
import os

virginia1 = open('Virginia1.txt', 'r')
virginia1 = virginia1.read()
virginia1 = virginia1.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia2 = open('Virginia2.txt', 'r')
virginia2 = virginia2.read()
virginia2 = virginia2.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

virginia3 = open('Virginia3.txt', 'r')
virginia3 = virginia3.read()
virginia3 = virginia3.replace("\r\n", " ").replace("\r", " ").replace("\n", " ").replace("  ", " ").replace("\"", " ")

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
sentences = sent_tokenizer.tokenize(virginia1) + sent_tokenizer.tokenize(virginia2) + sent_tokenizer.tokenize(virginia3)

sentences_select = []
sentences_done = []
level = 3
exclude = set(string.punctuation)

def rhyme(inp, level):
	entries = nltk.corpus.cmudict.entries()
	syllables = [(word, syl) for word, syl in entries if word == inp]
	rhymes = []
	for (word, syllable) in syllables:
		rhymes += [word for word, pron in entries if pron[-level:] == syllable[-level:]]
		return set(rhymes)

for sentence in sentences:
  	words = string.split(sentence)
  	wordCount = len(words)
	if wordCount == 10:
		sentences_select.append(sentence)

found = "NO"

x = 1
while x < 3:
	i = 1
	a = 1
	while i < 3:
		if i == 2:
			# print "started a rhyming loop", a
			words = string.split(sentence1)
			lastword1 = ''.join(ch for ch in words[9] if ch not in exclude)
			rhymewords = rhyme(lastword1, level)
			a += 1
			if rhymewords:
				for rhymeword in rhymewords:
					# print "selected rhymeword: ", rhymeword
					if lastword1 != rhymeword and found == "NO":
							#print "started looking at sentences to rhyme"
							for sentence2 in sentences_select:
								words2 = string.split(sentence2)
								lastword2 = ''.join(ch for ch in words2[9] if ch not in exclude)
								if lastword2 == rhymeword:
									found = "YES"
									print x, "------"
									print sentence1
									print sentence2
									i += 1
									x += 1
			
			# print "no rhymewords found; select another sentence"
			i = 1
				
		if i == 1:
			sentence1 = choice(sentences_select)
		 	if sentence1 not in sentences_done:
				# print "selected a sentence"
				sentences_done.append(sentence1)
				i += 1
