
Install Python, nltk. In a folder "1941" (or any other year of death), 
make folders for the authors of your choice. Each folder can contain as many 
texts as you can find in the Public Domain; texts should be in .txt format. 
If you want your publication to contain sources, include a bibliographic reference 
on the first line of each .txt document. 
Run deathoftheauthors.py from one level 
above the "1941" folder. The output is a simple .txt-file.

For output in .tex format (ConTeXt), download and run 
deathoftheauthors_tex.py plus header.txt instead. You can easily transform the generated 
files into pdfs by using <code>$ context *.tex</code>


Sourcematerial
Books used:

Sherwood Anderson
* Marching Men: http://en.wikipedia.org/wiki/Marching_Men 
* Poor White: http://en.wikipedia.org/wiki/Poor_White

Rabindranath Tagore 
* The Home and the World:http://en.wikipedia.org/wiki/The_Home_and_the_World 
* The Hungry Stones & other stories

Virginia Woolf
* Jacob's Room http://en.wikipedia.org/wiki/Jacob%27s_Room 
* Mrs Dalloway http://en.wikipedia.org/wiki/Mrs_dalloway 
* Night and Day http://en.wikipedia.org/wiki/Night_and_Day_(Woolf_novel)
* Orlando http://en.wikipedia.org/wiki/Orlando:_A_Biography
* The Voyage Out http://en.wikipedia.org/wiki/The_Voyage_Out 
* To the Lighthouse http://en.wikipedia.org/wiki/To_the_lighthouse

Elizabeth von Arnim
* Elizabeth and her German Garden http://en.wikipedia.org/wiki/Elizabeth_and_Her_German_Garden
* Vera http://en.wikipedia.org/wiki/Vera_(novel)

Henri Bergson
* Dreams
* Laughter and the meaning of the comic http://en.wikipedia.org/wiki/Laughter:_An_Essay_on_the_Meaning_of_the_Comic
* The meaning of war


James Joyce
* Portrait of an artist as a young man http://en.wikipedia.org/wiki/James_Joyce#A_Portrait_of_the_Artist_as_a_Young_Man 
* Ulysses http://en.wikipedia.org/wiki/Ulysses_(novel)

