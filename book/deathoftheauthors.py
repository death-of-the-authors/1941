#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
from random import *
import time
import nltk
from random import shuffle

# VARIABLES

year = '1941'
now = time.strftime("%Y-%m-%d_%H:%M:%S")
filename = year+'_'+now+'.txt'
authors =  os.listdir(year) # Make list of authors based on folder names

random_author = ""
duo = []
author_texts = []
texts = []

season_chapter = []
spring_chapter = []
summer_chapter = []
autumn_chapter = []
winter_chapter = []

season_select = []
spring_select = []
summer_select = []
autumn_select = []
winter_select = []

# FUNCTIONS

# Select a random duo of two authors (no repeats)

def select_duo(authors):
    i = 0
    while i < 2:
	    if authors != []:
		    pos = randrange(len(authors))
		    random_author = authors[pos]
		    authors[pos] = authors[-1]
		    del authors[-1]
		    duo.append(random_author)
		    i += 1
    return duo


# Select one random text per author

def select_texts(duo):   
    for author in duo:
	    path = year+"/"+author
	    author_texts = os.listdir(path)
	    pos = randrange(len(author_texts))
	    random_text = path+"/"+author_texts[pos]
	    texts.append(random_text)
    return texts


# Select 20 random keywords from seasonal lists

def select_keywords(season):
    i = 0
    while i < 10:
        if season != []:
            pos = randrange(len(season))
            word = season[pos]
            season[pos] = season[-1]
            del season[-1]
            season_select.append(word)
            i += 1
    return season_select                     


# Check if sentence contains seasonal keyword, if so, generate fake paragraps and combine sentences into chapters

def select_sentences(season_select):
    for sentence in sentences:
        wordlist = sentence.split(" ")
        for word in wordlist:
            i = 0
            if word in season_select:
                i += 1
                sentence = sentence+' '
                if i == randint(1,6):
                    sentence = sentence+"\n\n"
                    i = 0
                season_chapter.append(sentence)
    return season_chapter


# Write remix novel

    # write text in file

def writetoLog(content):
		try:
			logfile = open(filename, "a")
			try:
				logfile.write(content)
			finally:
				logfile.close()
		except IOError:
			pass


    # print title chapter

i = ""
def print_chapter(i):
    writetoLog('\n\n\n'+i+'\n\n')


    # add sentences chapter

def print_sentences(season_chapter):
    for sentence in season_chapter:
        writetoLog(sentence)



# SCRIPT

# Select a random duo of two authors (no repeats)

select_duo(authors)
print(duo)

# select one random text per author

select_texts(duo)
print(texts)

# Read files & clean up

for source in texts:
	text = open(source, 'r')
	text = text.read()
	text = text.replace("\r\n", " ")
	text = text.replace("\r", " ")
	text = text.replace("\n", " ")
	text = text.replace("  ", " ")
    
    
# Split text into sentences with help of nltk
for source in texts:
    sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
    sentences = sent_tokenizer.tokenize(text)


# Seasonal keywords

spring = ['birth','butterfly','flower','energy', 'breakthrough', 'sprawl', 'toddler', 'grow', 'plasticine', 'toy', 'hope', 'teenager', 'puberty', 'adolescence', 'sneakers', 'acne', 'pimple', 'jitters', 'first time', 'slang', 'verdant', 'leafy', 'juvenile', 'inexperienced', 'girlish', 'young', 'verginal', 'ingenuous', 'naive', 'innocent', 'launch', 'genesis', 'youth', 'vibration', 'dynamic', 'dynamism', 'vibrance', 'empowerment', 'enhancement', 'innovation', 'invention', 'play', 'await', 'blessing', 'fresh', 'small', 'spring', 'morning', 'sunrise', 'dawn']

summer = ['sun', 'heat', 'growth','expansion', 'develop', 'unfold', 'radiance', 'attempt', 'succeed', 'stumble', 'bloom', 'blossom', 'light', 'persistence', 'grace', 'mature' , 'strength' , 'method', 'serendipity', 'success', 'summit', 'zenith', 'glory', 'grandeur', 'splendor', 'belle epoque', 'power', 'bright', 'ripe', 'summer', 'rose', 'harmony', 'midday', 'full', 'birds', 'fashion', 'class', 'winner', 'victory', 'triumph', 'realization', 'diamond', 'precious', 'excitement', 'gold', 'beauty', 'fire']

autumn = ['leaves', 'decay', 'grey', 'gray','age','generation', 'calmness', 'sofa', 'pipe', 'mist','memory', 'remembrance', 'nostalgia', 'picture', 'cigar', 'painting', 'museum','indian summer',  'peace', 'sharing', 'teaching', 'harvest', 'slow', 'stiff', 'waste', 'autumn', 'sunset', 'dusk', 'evening', 'shadow', 'gloaming', 'twilight', 'feather', 'quiet', 'serenity', 'stillness', 'tranquility', 'rest', 'relaxed', 'silence', 'library', 'thought', 'experience', 'wise', 'wisdom', 'reflective', 'literate', 'serious']

winter = ['death', 'ice', 'cold','illness', 'death', 'ruin', 'decay', 'medicine', 'pus', 'infection', 'virus', 'nurse', 'hospital','funeral', 'coffin', 'soil', 'rat', 'skull', 'bones', 'skeleton','humus', 'transformation', 'symbiosis', 'seed', 'egg', 'offspring', 'sprout', 'pain', 'deteriorate', 'regress', 'retrogress', 'return', 'alter', 'change', 'metamorphose', 'modify', 'mutate', 'transfigure', 'transform', 'transmute', 'winter', 'snow', 'night', 'hell', 'heaven', 'earth','archives', 'replica', 'sorrow', 'grief', 'old']


# Select 20 random keywords from seasonal lists

select_keywords(spring)
spring_select = season_select

select_keywords(summer)
summer_select = season_select

select_keywords(autumn)
autumn_select = season_select

select_keywords(winter)
winter_select = season_select

# Check if sentence contains seasonal keyword, if so, generate fake paragraps and combine sentences into chapters

select_sentences(spring_select)
spring_chapter = season_chapter

select_sentences(summer_select)
summer_chapter = season_chapter

select_sentences(autumn_select)
autumn_chapter = season_chapter

select_sentences(winter_select)
winter_chapter = season_chapter


# shuffle chapters

shuffle(spring_chapter)
shuffle(summer_chapter)
shuffle(autumn_chapter)
shuffle(winter_chapter)


# write to remix novel

# Open file and write book with title page, chapter page, chapters, colophon

author_duo = (str(duo[0])+' & '+str(duo[1])).replace("_", " ")

writetoLog('The Death of the Authors\n')
writetoLog(author_duo+'\n')
writetoLog('& Their Return to Life in Four Seasons\n')
writetoLog('A Constant Remix\n\n\n')

# print chapters

print_chapter("Spring")      
print_sentences(spring_chapter)

print_chapter("Summer")      
print_sentences(summer_chapter)

print_chapter("Autumn")      
print_sentences(autumn_chapter)

print_chapter("Winter")      
print_sentences(winter_chapter)

# Add sources

writetoLog("\n\n\nColophon\n\n")
writetoLog("Sources:\n")

for text in texts:
	credits = open(text,'r').readlines()[:1]

	for credit in credits:

		writetoLog(credit+'\n ')

writetoLog('This book was generated on '+now+' from sources available in the Public Domain as of '+str(int(year) + 71)+', 70 years after the death of '+author_duo+'.\n\nRead more at:\nwww.publicdomainday.org\nwww.constantvzw.org/publicdomainday')


